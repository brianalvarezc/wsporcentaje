FROM node:14-alpine
LABEL maintainer="Brian Alvarez balvarez@extreme.com.co"
COPY package*.json .
RUN npm install
COPY . .
WORKDIR /
EXPOSE 3000
CMD ["node", "server.js"]