const express = require('express');
const { Pool } = require('pg');

const app = express();

const pool = new Pool({
  host: 'db.essmar.extreme.com.co',
  user: 'test',
  password: '3xtr3m3',
  database: 'kaguaprod',
  port: 5432,
});

app.get('/ultimaFactura', (req, res) => {
  const periodo = req.query.periodo;
  
  const consulta = `with ultfact as (select max(conteo) conteo, count(1) cant
                      from temp_new_factura_cab 
                      where (cod_peri = ${periodo} or ${periodo} = 0))
                    select tnfc.cod_pred, tnfc.nro_factura, uf.cant, tnfc.proceso
                    from temp_new_factura_cab tnfc
                    inner join ultfact uf on uf.conteo = tnfc.conteo;`;

  pool.query(consulta, (err, result) => {
    if (err) {
      res.status(500).send(`Error en la consulta: ${err}`);
    } else {
      res.json(result.rows);
    }
  });
});

app.get('/limpiarTabla', (req, res) => {
  const periodo = req.query.periodo;
  
  const consulta = `delete from temp_new_factura_cab where (${periodo} = 0 or cod_peri = ${periodo});
  SELECT setval(pg_get_serial_sequence('public.temp_new_factura_cab', 'conteo'), 1);`;

  pool.query(consulta, (err, result) => {
    if (err) {
      res.status(500).send(`Error en la consulta: ${err}`);
    } else {
      res.json(result.rows);
    }
  });
});

app.listen(3000, () => {
  console.log('Servidor iniciado en puerto 3000');
});